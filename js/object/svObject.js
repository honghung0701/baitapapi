function Student (_ma,_ten,_email,_matKhau,_diemToan,_diemLy,_diemHoa) {
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.matKhau = _matKhau;
    this.toan = _diemToan;
    this.ly = _diemLy;
    this.hoa = _diemHoa;
    this.mediumScore = function () {
        var medium = (this.toan*1 + this.ly*1 + this.hoa*1) / 3;
        return medium.toFixed(1);
    }
}