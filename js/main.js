const BASE_URL = "https://633ec05d0dbc3309f3bc54a6.mockapi.io";

// var batLoading = function(){
//     document.getElementById("loading").style.display = "flex";
// }
// var tatLoading = function(){
//     document.getElementById("loading").style.display = "none";
// }


var fetchDssvService = function() {
    // batLoading()
    axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    })
        .then(function(res) {
            renderDssv(res.data);
            // tatLoading();
        })
        .catch(function(err) {
            // tatLoading();
        })   
}
fetchDssvService();

// render danh sách sinh viên
var renderDssv = function(listSv) {
    var contentHTML = "";
    listSv.forEach(function(sv) {
        contentHTML+=
        `<tr>
            <td>${sv.ma}</td>
            <td>${sv.ten}</td>
            <td>${sv.email}</td>
            <td>0</td>
            <td>
            <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xóa</button>
            <button onclick="suaSv(${sv.ma})" class="btn btn-primary">Sửa</button>
            </td>
        </tr>`;
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
var xoaSv = function(idSv) {
    // batLoading();
    axios({
        url: `${BASE_URL}/sv/${idSv}`,
        method: "DELETE",
    })
        .then(function(res) {
            fetchDssvService();
            // tatLoading();
            Swal.fire('Xóa thành công')
        })
        .catch(function(err) {
            // tatLoading();
            Swal.fire('Xóa thất bại')

        })
}
function themSv() {
    var sv = informationForm();
    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: sv,
    })
        .then(function(res) {
            Swal.fire('Thêm thành công');
            fetchDssvService();
        })
        .catch(function (err) {
            Swal.fire('Thêm thất bại');
        })
}
var suaSv = function(idSv) {
    axios({
        url: `${BASE_URL}/sv/${idSv}`,
        method: "GET",
    })
    .then(function(res) {
        var sv = res.data;
        showInformationForm(idSv);
    })
    .catch(function(err) {
        console.log(err);
    })
}
var capNhatSv = function () {
    var sv = informationForm()  
    axios({
        url: `${BASE_URL}/sv/${sv.ma}`,
        method: "PUT",
        data: sv,
    })
    .then(function(res) {
        Swal.fire('Cập nhật thành công');
        fetchDssvService();
    })
    .catch(function(err) {
        Swal.fire('Cập nhật thất bại');
    })
}