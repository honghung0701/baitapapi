function informationForm() {
    var maSv = document.getElementById('txtMaSV').value.trim();
    var tenSv = document.getElementById('txtTenSV').value.trim();
    var email = document.getElementById('txtEmail').value.trim();
    var matKhau = document.getElementById('txtPass').value.trim();
    var toan = document.getElementById('txtDiemToan').value.trim();
    var ly = document.getElementById('txtDiemLy').value.trim();
    var hoa = document.getElementById('txtDiemHoa').value.trim();
    var students = new Student (maSv,tenSv,email,matKhau,toan,ly,hoa);
    return students;
}
function showInformationForm(sinhVien) {
  document.getElementById("txtMaSV").value = sinhVien.ma;
  document.getElementById("txtTenSV").value = sinhVien.ten;
  document.getElementById("txtEmail").value = sinhVien.email;
  document.getElementById("txtPass").value = sinhVien.matKhau;
  document.getElementById("txtDiemLy").value = sinhVien.ly;
  document.getElementById("txtDiemHoa").value = sinhVien.hoa;
  document.getElementById("txtDiemToan").value = sinhVien.toan;
}
